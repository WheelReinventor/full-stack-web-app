package net.iamverysmart.blogmanager.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.iamverysmart.blogmanager.dto.BlogDTO;
import net.iamverysmart.blogmanager.entity.Blog;
import net.iamverysmart.blogmanager.exceptions.BlogException;
import net.iamverysmart.blogmanager.exceptions.ErrorCode;
import net.iamverysmart.blogmanager.service.BlogService;

@CrossOrigin
@RestController
@RequestMapping("/blogs")
public class BlogController {

	@Autowired
	private BlogService blogService;
	
	private static final Logger logger = LoggerFactory.getLogger(BlogController.class);

	@GetMapping("")
	@ResponseBody
	public List<BlogDTO> getAllBlogs() {
		return blogService.getAllBlogs();
	}
	
	@PostMapping("")
	@ResponseBody
	public  BlogDTO createBlog(@RequestBody BlogDTO blogDTO) {
		Blog blog = blogDTO.toEntity();
		blogService.createBlog(blog);
		
		return BlogDTO.prepareDTO(blog);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBlog(@PathVariable Long id) {
		if (blogService.deleteBlogById(id)) {
			logger.debug("Blog with id " + id + " deleted from the database");
		} else {
			throw new BlogException(ErrorCode.BLOG_NOT_FOUND);
		}
	}
	
	@ExceptionHandler(BlogException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public String handleBlogException(BlogException e) {
	    return e.getMessage();
	}

}
