package net.iamverysmart.blogmanager.dto;

import java.time.LocalDateTime;

import net.iamverysmart.blogmanager.entity.Blog;

public class BlogDTO {
    private Long id;
    private String title;
    private String content;
    private LocalDateTime createdDate;

    public BlogDTO(Long id, String title, String content, LocalDateTime createdDate) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }
    
	public Blog toEntity() {
        Blog blog = new Blog();
        blog.setTitle(this.title);
        blog.setContent(this.content);
        blog.setCreatedDate(this.createdDate);
        return blog;
    }
    
    public static BlogDTO prepareDTO(Blog blog) {
        return new BlogDTO(blog.getId(), blog.getTitle(), blog.getContent(), blog.getCreatedDate());
    }
}
