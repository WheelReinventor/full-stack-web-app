package net.iamverysmart.blogmanager.exceptions;

public class BlogException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private ErrorCode errorCode;
	
	public BlogException(ErrorCode errorCode) {
		super(errorCode.getMessage());
		this.errorCode = errorCode;
	}
	
	public ErrorCode getErrorCode() {
		return errorCode;
	}
}
