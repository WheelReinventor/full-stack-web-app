package net.iamverysmart.blogmanager.exceptions;

public enum ErrorCode {
	BLOG_NOT_FOUND("Blog not found");
	
	private final String message;
	
	ErrorCode(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
