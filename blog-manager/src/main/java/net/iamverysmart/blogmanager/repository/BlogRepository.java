package net.iamverysmart.blogmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.iamverysmart.blogmanager.entity.Blog;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {
	Blog findById(long id);
}
