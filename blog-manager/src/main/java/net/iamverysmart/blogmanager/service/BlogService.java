package net.iamverysmart.blogmanager.service;

import java.util.List;

import net.iamverysmart.blogmanager.dto.BlogDTO;
import net.iamverysmart.blogmanager.entity.Blog;

public interface BlogService {
	List<BlogDTO> getAllBlogs();
	BlogDTO createBlog(Blog blog);
	Boolean deleteBlogById(Long id);
}
