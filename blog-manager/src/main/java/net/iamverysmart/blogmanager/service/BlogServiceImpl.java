package net.iamverysmart.blogmanager.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.iamverysmart.blogmanager.dto.BlogDTO;
import net.iamverysmart.blogmanager.entity.Blog;
import net.iamverysmart.blogmanager.exceptions.BlogException;
import net.iamverysmart.blogmanager.exceptions.ErrorCode;
import net.iamverysmart.blogmanager.repository.BlogRepository;

@Service
public class BlogServiceImpl implements BlogService {

	@Autowired
	BlogRepository blogRepository;

	private static final Logger logger = LoggerFactory.getLogger(BlogServiceImpl.class);

	public List<BlogDTO> getAllBlogs() {
		List<BlogDTO> blogDTOs = new ArrayList<>();
		List<Blog> blogs = blogRepository.findAll();
		if (blogs == null) {
			throw new BlogException(ErrorCode.BLOG_NOT_FOUND);
		}
		blogs.forEach(blog -> {
			blogDTOs.add(BlogDTO.prepareDTO(blog));
		});
		return blogDTOs;
	}

	public BlogDTO createBlog(Blog blog) {
		blog.setCreatedDate(LocalDateTime.now());
		Blog savedBlog = blogRepository.save(blog);
		return BlogDTO.prepareDTO(savedBlog);
	}

	public Boolean deleteBlogById(Long id) throws BlogException {
		logger.debug("Deleting blog with id " + id + " from the database");
		Optional<Blog> blog = blogRepository.findById(id);
		if (blog.isPresent()) {
			blogRepository.deleteById(id);
			return true;
		} else {
			throw new BlogException(ErrorCode.BLOG_NOT_FOUND);
		}
	}

}
