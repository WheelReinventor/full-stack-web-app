CREATE TABLE blog_posts (
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  content TEXT NOT NULL,
  created_at DATETIME NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO blog_posts (title, content, created_at) VALUES
('First Post', 'This is my first blog post.', '2022-01-01 00:00:00'),
('Second Post', 'This is my second blog post.', '2022-01-02 00:00:00'),
('Third Post', 'This is my third blog post.', '2022-01-03 00:00:00'),
('Fourth Post', 'This is my fourth blog post.', '2022-01-04 00:00:00'),
('Fifth Post', 'This is my fifth blog post.', '2022-01-05 00:00:00');
