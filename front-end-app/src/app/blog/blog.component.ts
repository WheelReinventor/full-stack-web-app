import { Component, OnInit } from '@angular/core';
import { BlogService } from './blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  blogs!: Array<any>;
  newestFirst: boolean = false;
  showForm: boolean = false;
  newPost: any = {};

  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.blogService.getBlogs().subscribe((data: Array<any>) => {
      this.sortBlogs(data);
    });
  }

  toggleSortOrder() {
    this.newestFirst = !this.newestFirst;
    this.sortBlogs(this.blogs);
  }

  private sortBlogs(data: Array<any>) {
    this.blogs = data.sort((a: any, b: any) => {
      const dateA = new Date(a.createdDate).getTime();
      const dateB = new Date(b.createdDate).getTime();
      return this.newestFirst ? dateB - dateA : dateA - dateB;
    });
  }

  toggleCreatePostForm() {
    this.showForm = !this.showForm;
    this.newPost.title = '';
    this.newPost.content = '';
  }

  onCreatePost() {
    this.blogService.createBlog(this.newPost).subscribe((data: any) => {
      this.blogs.unshift(data);
      this.sortBlogs(this.blogs);
      this.newPost = {};
      this.showForm = false;
    });
  }

  onDeletePost(id: number) {
    this.blogService.deleteBlog(id).subscribe(() => {
      this.blogs = this.blogs.filter((blog: any) => blog.id !== id);
    });
  }
}
