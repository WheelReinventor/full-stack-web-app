import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  private apiUrl = 'http://localhost:8080/blogs';

  constructor(private http: HttpClient) { }

  getBlogs() {
    return this.http.get<Array<any>>(this.apiUrl);
  }

  createBlog(blog: any) {
    return this.http.post(this.apiUrl, blog);
  }

  deleteBlog(id: number) {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
